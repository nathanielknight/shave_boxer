module Stages.Abiogenesis exposing (..)

import Dict
import List

import Common exposing (..)

cards : List Card
cards =
    [ { title = "Meteor Impact"
      , modify = (\p -> { p | temperature = clamp 0 10 (p.temperature + 1)})
      }
    , { title = "Comet Impact"
      , modify = (\p -> { p |
                          temperature = clamp 0 10 (p.temperature + 1)
                        , hydration = clamp 0 10 (p.hydration + 1)
                        })
      }
    , { title = "Aerosol Buildup"
      , modify = (\p -> { p | temperature = clamp 0 10 (p.temperature - 1) })
      }
    , { title = "Atmospheric Depletion"
      , modify = (\p -> { p |
                          temperature = clamp 0 10 (p.temperature - 1)
                        , hydration = clamp 0 10 (p.hydration - 1)
                        })
      }
    ]

abiogenesis : Stage
abiogenesis =
    let
        progress p = List.foldr
                     (&&)
                     False
                     [ p.temperature > 5
                     , p.temperature < 9
                     , p.hydration > 4
                     , p.hydration < 10
                     ]
    in 
        { game = { planet = {hydration = 0, temperature = 0}
                 , cards = Dict.fromList
                           <| List.indexedMap (\i c -> (i, c)) cards
                 }
        , progress = progress
        , nextStage = NoStage
        }
        
