module Main exposing (..)

import Dict
import List
import Maybe
import Random

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Html.App

import Common exposing (Game, Card)
import Stages.Abiogenesis as Abiogenesis


-- Common helpers


cardGenerator : List Card -> Random.Generator Card
cardGenerator cards =
    let
        cs = Dict.fromList
             <| List.indexedMap (\i c -> (i, c)) cards
        num = List.length cards
        getCard i =
            Dict.get i cs
            |> Maybe.withDefault
               { title = "No card for " ++ (toString i)
               , modify = (\p -> p)
               }

    in
        Random.map getCard <| Random.int 0 (num - 1)

addCard : Dict.Dict Int Card -> Card -> Dict.Dict Int Card
addCard cards card =
    let
        newIndex = cards
                 |> Dict.keys
                 |> List.maximum
                 |> Maybe.withDefault 0
                 |> (+) 1
    in
        Dict.insert newIndex card cards

showCard : (Int, Card) -> Html Msg
showCard (i, c) =
    p [class "card"
      , onClick (PlayCard i c)
      ]
    [text c.title]



-- Elm Framework Components
type alias Model = { game : Game }

init : (Model, Cmd msg)
init =
    ( { game = { planet = { temperature = 0
                          , hydration = 0
                          }
               , cards = Dict.fromList
                         <| List.indexedMap (\i c -> (i, c)) Abiogenesis.cards
               }
      }
    , Cmd.none
    )

type Msg = PlayCard Int Card
         | DrawCard Card

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        PlayCard i c ->
            let
                newPlanet = c.modify model.game.planet
                newCards = Dict.remove i model.game.cards
                newModel = { game = { planet = newPlanet
                                    , cards = newCards
                                    }
                           }
            in
                (newModel, Random.generate DrawCard (cardGenerator Abiogenesis.cards))

        DrawCard c ->
            let
                game = model.game
                newCards = addCard model.game.cards c
                newGame = { game | cards = newCards }
                newModel = {game = newGame}
            in 
                ( newModel, Cmd.none)

view : Model -> Html Msg
view model =
    div [id "app"]
        [ div [id "planet-stats"]
              [ p [] [text ("Temperature: " ++ (toString model.game.planet.temperature))]
              , p [] [text ("Hydration: " ++ (toString model.game.planet.hydration))]
              ]
        , div [id "cards"] ( model.game.cards
                           |> Dict.toList
                           |> List.map showCard)
        ]
            

main : Program Never
main =
    Html.App.program
        { init = init
        , update = update
        , view = view                   
        , subscriptions = (\m -> Sub.none)
        }
       
