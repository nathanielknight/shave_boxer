module Common exposing (..)

import Dict

-- Primitive Types
type alias Card =
    { title : String
    , modify : Planet -> Planet
    }

type alias Planet =
    { temperature : Int
    , hydration: Int
    }

type alias Game =
    { planet : Planet
    , cards : Dict.Dict Int Card
    }

-- Container Types
type StagePointer = NoStage
                  | Next Stage
type alias Stage = { game : Game
                   , progress : (Planet -> Bool)
                   , nextStage : StagePointer
                   }

