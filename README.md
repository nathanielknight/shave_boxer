# Project Codename: `Shave Boxer`

A game inspired by the Fermi paradox.

In this game the player shepherds a planet through a few stages on a
possible path towards hosting a sentient species in a galactic
community. Namely:

 - life arising
 - complex life arising
 - sustainable intelligent life arising

Naturally, at each stage there will be some attrition as planets end
up uninhabited or devoid of sentient life.


Basic Game
==========

There is a planet with some statistics (depending on the stage) and a
number of "cards". The player selects a card, which alters the planets
statistics, and depending on the value of the statistics the game has
a chance of progressing to the next stage or ending.

Mechanics
---------

The planet has statistics. Cards change statistics. Cards are drawn at
raondom. Each stage has criteria for progression or termination.

Dynamics
--------

Since only a finite number of cards are available, players may have to
play cards they don't want. Managing the number of undesirable cards
in one's hand is necessary, or the planet will be pushed into an
undesirable ending.

Aesthetics
----------

This game participates in:

 - *Narrative*: it follows the grand story of an alien world.
 - *Challenge*: it follows semi-predictable rules and has many failure
   states.
 - *Discovery*: each stage offers new and more complex posibilities.
